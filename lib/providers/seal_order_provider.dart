// ignore_for_file: unnecessary_new, unused_field, prefer_final_fields, unnecessary_getters_setters, avoid_print, unnecessary_brace_in_string_interps

import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:future_job_anur/models/seal_order_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class SealOrderProvider with ChangeNotifier {
  int _assignDataQty = 0;

  int get assignDataQty => _assignDataQty;

  set assignDataQty(int newVal) {
    _assignDataQty = newVal;
    notifyListeners();
  }

  Future<Map?> getSealOrder() async {
    try {
      var uriStr = "http://127.0.0.1:3000/sealOrder-driverApp";
      uriStr =
          "http://103.234.195.44:3001/api/v1/driverApp/getOrder?driverId=DRVR-20220203-0001";
      var response = await http.get(Uri.parse(uriStr));

      print(
          "=====================================>>>>>>  ========= ${response.statusCode}");
      print(response.body);

      if (response.statusCode == 200) {
        SealOrderModel sealMod =
            SealOrderModel.fromJson(jsonDecode(response.body));

        for (var i = 0; i < 10; i++) {
          AssignmentDatum assignData = sealMod.data.assignmentData[0];
          sealMod.data.assignmentData.add(assignData);
        }
        for (var i = 0; i < 10; i++) {
          AssignmentDatum assignData = sealMod.data.assignmentData[0];
          sealMod.data.onPickData.add(assignData);
        }
        sealMod.data.onPickDataQty = sealMod.data.onPickData.length;

        print(
            "sealMod.data.assignmentData.length ${sealMod.data.assignmentData.length}");

        // assignDataQty = sealMod.data.assignmentData.length;

        print("_assignDataQty ... ${_assignDataQty}");
        print(" assignDataQty ... ${assignDataQty}");
        var mapOrder = new Map();
        mapOrder['sealMod'] = sealMod;
        mapOrder['onProgressQty'] = 12;

        return mapOrder;
      } else {
        return null;
      }
    } catch (e) {
      print("===================> ${e}");
      return null;
    }
  }

  Future<Object?> sendData(File imageFile) async {
    try {
      var postUri = Uri.parse(
          "http://103.234.195.44:3001/api/v1/driverApp/uploadPhoto?sealContainerId=232&status=1");

      http.MultipartRequest request =
          new http.MultipartRequest("POST", postUri);
      print("DISINI=====================>" + basename(imageFile.path));
      // http.MultipartFile multipartFile =
      //     await http.MultipartFile.fromPath('file1', filePath);

      var stream =
          new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
      var length = await imageFile.length();
      var multipartFilevar =
          new http.MultipartFile('file1', stream, length, filename: "xxx.jpg");
      print(multipartFilevar);
      request.files.add(multipartFilevar);

      http.StreamedResponse response = await request.send();
      var responseData = await response.stream.toBytes();
      var result = String.fromCharCodes(responseData);
      print("response ===>" + result);
    } catch (e) {
      print(e);
      return null;
    }
  }
}
