// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:future_job_anur/pages/eseal/get_photo.dart';
import 'package:future_job_anur/pages/eseal/seal_detail.dart';
import 'package:future_job_anur/pages/home_page.dart';
import 'package:future_job_anur/pages/onboarding_page.dart';
import 'package:future_job_anur/pages/sign_in_page.dart';
import 'package:future_job_anur/pages/sign_up_page.dart';
import 'package:future_job_anur/pages/splash_page.dart';
import 'package:future_job_anur/providers/auth_provider.dart';
import 'package:future_job_anur/providers/seal_order_provider.dart';
import 'package:future_job_anur/providers/user_provider.dart';
import 'package:future_job_anur/pages/eseal/home_seal.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AuthProvider>(
            create: (context) => AuthProvider()),
        ChangeNotifierProvider<UserProvider>(
            create: (context) => UserProvider()),
        ChangeNotifierProvider<SealOrderProvider>(
            create: (context) => SealOrderProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: {
          '/': (context) => LandingPage(), //GetPhoto(),
          '/sign-up': (context) => SignUpPage(),
          '/home': (context) => HomePage(),
          '/sign-in': (context) => SignInPage(),
          '/seal-detail': (context) => SealDetail(),
          '/home-seal': (context) => HomeSeal(),
          '/onboarding': (context) => OnboardingPage()
        },
      ),
    );
  }
}

checkIfAuthenticated() async {
  await Future.delayed(Duration(
      seconds: 4)); // could be a long running task, like a fetch from keychain
  return false;
}

class LandingPage extends StatelessWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    checkIfAuthenticated().then((success) {
      if (success) {
        Navigator.pushReplacementNamed(context, '/home');
      } else {
        Navigator.pushReplacementNamed(context, '/sign-in');
      }
    });

    return SplashPage();
    // Scaffold(
    //   backgroundColor: Colors.white,
    //   body: Center(
    //     child: CircularProgressIndicator(),
    //   ),
    // );
  }
}
