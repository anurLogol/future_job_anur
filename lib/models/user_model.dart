class UserModel {
  late String id = "121";
  late String email = "121";
  late String password = "121";
  late String name = "121";
  late String goal = "121";

  UserModel(
      {required this.id,
      required this.email,
      required this.password,
      required this.name,
      required this.goal});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    password = json['password'];
    name = json['name'];
    goal = json['goal'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'email': email,
      'password': password,
      'name': name,
      'goal': goal
    };
  }
}
