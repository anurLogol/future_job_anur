// To parse this JSON data, do
//
//     final sealOrderModel = sealOrderModelFromJson(jsonString);

import 'dart:convert';

SealOrderModel sealOrderModelFromJson(String str) =>
    SealOrderModel.fromJson(json.decode(str));

String sealOrderModelToJson(SealOrderModel data) => json.encode(data.toJson());

class SealOrderModel {
  SealOrderModel({
    required this.isSuccess,
    required this.data,
  });

  bool isSuccess;
  Data data;

  factory SealOrderModel.fromJson(Map<String, dynamic> json) => SealOrderModel(
        isSuccess: json["isSuccess"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "isSuccess": isSuccess,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.assigmentQty,
    required this.onPickDataQty,
    required this.assignmentData,
    required this.onPickData,
  });

  int assigmentQty;
  int onPickDataQty;
  List<AssignmentDatum> assignmentData;
  List<dynamic> onPickData;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        assigmentQty: json["assigmentQty"],
        onPickDataQty: json["onPickDataQty"],
        assignmentData: List<AssignmentDatum>.from(
            json["assignmentData"].map((x) => AssignmentDatum.fromJson(x))),
        onPickData: List<dynamic>.from(json["onPickData"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "assigmentQty": assigmentQty,
        "onPickDataQty": onPickDataQty,
        "assignmentData":
            List<dynamic>.from(assignmentData.map((x) => x.toJson())),
        "onPickData": List<dynamic>.from(onPickData.map((x) => x)),
      };
}

class AssignmentDatum {
  AssignmentDatum({
    required this.sealRequestContainerId,
    required this.orderType,
    required this.pickUpFrom,
    required this.destination,
    required this.deliveryOrderId,
    required this.ajuNumber,
    required this.nomorPendaftaran,
    required this.timingReceiveAssignment,
    required this.containerNo,
    required this.containerSizeType,
    required this.tanggalBongkar,
    required this.driverAssignmentStatus,
  });

  int sealRequestContainerId;
  String orderType;
  String pickUpFrom;
  String destination;
  String deliveryOrderId;
  String ajuNumber;
  String nomorPendaftaran;
  String timingReceiveAssignment;
  String containerNo;
  String containerSizeType;
  DateTime tanggalBongkar;
  int driverAssignmentStatus;

  factory AssignmentDatum.fromJson(Map<String, dynamic> json) =>
      AssignmentDatum(
        sealRequestContainerId: json["SealRequestContainerID"],
        orderType: json["orderType"],
        pickUpFrom: json["pickUpFrom"],
        destination: json["destination"],
        deliveryOrderId: json["deliveryOrderID"],
        ajuNumber: json["ajuNumber"],
        nomorPendaftaran: json["nomorPendaftaran"],
        timingReceiveAssignment: json["TimingReceiveAssignment"],
        containerNo: json["containerNo"],
        containerSizeType: json["containerSizeType"],
        tanggalBongkar: DateTime.parse(json["tanggalBongkar"]),
        driverAssignmentStatus: json["driverAssignmentStatus"],
      );

  Map<String, dynamic> toJson() => {
        "SealRequestContainerID": sealRequestContainerId,
        "orderType": orderType,
        "pickUpFrom": pickUpFrom,
        "destination": destination,
        "deliveryOrderID": deliveryOrderId,
        "ajuNumber": ajuNumber,
        "nomorPendaftaran": nomorPendaftaran,
        "TimingReceiveAssignment": timingReceiveAssignment,
        "containerNo": containerNo,
        "containerSizeType": containerSizeType,
        "tanggalBongkar": tanggalBongkar.toIso8601String(),
        "driverAssignmentStatus": driverAssignmentStatus,
      };
}
