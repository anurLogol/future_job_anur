// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unused_local_variable, unnecessary_new

import 'package:flutter/material.dart';
import 'package:future_job_anur/models/seal_order_model.dart';
import 'package:future_job_anur/providers/seal_order_provider.dart';
import 'package:future_job_anur/theme.dart';
import 'package:provider/provider.dart';

class HomeSeal extends StatefulWidget {
  @override
  _HomeSealState createState() => _HomeSealState();
}

class _HomeSealState extends State<HomeSeal>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<String> sealCardCol3 = <String>['Do No.', 'Dok. Bea Cukai'];
  var onProgressCount = 0;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
      child: Container(
          child: Row(
        children: [
          Text(
            "APP",
            style: TextStyle(
                color: Colors.blue[900],
                fontSize: 30,
                fontWeight: FontWeight.bold),
          ),
          Spacer(),
          Row(
            children: [
              Text(
                "Logouts",
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
              ),
              Icon(Icons.logout)
            ],
          )
        ],
      )),
    );
  }

  Widget onProgressWidget(AssignmentDatum assignmentDatum) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/seal-detail');
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Icon(
                          Icons.lock,
                          color: Colors.blue[900],
                        ),
                        Text(
                          // assignmentDatum.orderType,
                          "xxxx",
                          style: sealCardValTextStyle,
                        )
                      ],
                    ),
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 4),
                        child: Text(
                          "1 min ago",
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.blue[900],
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(5),
                                bottomLeft: Radius.circular(5))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 2, horizontal: 10),
                          child: Text(
                            "E-Seal",
                            style: TextStyle(color: Colors.white, fontSize: 10),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Tujuan",
                      style: sealCardLblTextStyle,
                    ),
                    Text(
                      // assignmentDatum.destination,
                      "yyyyy",
                      style: sealCardValTextStyle,
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Divider(color: greyColor),
              ),
              Row(
                children: sealCardCol3.map(
                  (title) {
                    var index = sealCardCol3.indexOf(title);

                    var dataVal = index == 0
                        ? assignmentDatum.ajuNumber
                        : assignmentDatum.deliveryOrderId;
                    dataVal = "data dummy";
                    return Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              title,
                              style: sealCardLblTextStyle,
                            ),
                            Text(
                              dataVal,
                              style: sealCardValTextStyle,
                            )
                          ],
                        ));
                  },
                ).toList(),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget availableWidget(dynamic assignmentDatum) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/seal-detail');
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Icon(
                          Icons.lock,
                          color: Colors.blue[900],
                        ),
                        Text(
                          assignmentDatum.orderType,
                          style: sealCardValTextStyle,
                        )
                      ],
                    ),
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 4),
                        child: Text(
                          "1 min ago",
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.blue[900],
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(5),
                                bottomLeft: Radius.circular(5))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 2, horizontal: 10),
                          child: Text(
                            "label-x",
                            style: TextStyle(color: Colors.white, fontSize: 10),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Tujuan",
                      style: sealCardLblTextStyle,
                    ),
                    Text(
                      assignmentDatum.destination,
                      style: sealCardValTextStyle,
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Divider(color: greyColor),
              ),
              Row(
                children: sealCardCol3.map(
                  (title) {
                    var index = sealCardCol3.indexOf(title);

                    var dataVal = index == 0
                        ? assignmentDatum.ajuNumber
                        : assignmentDatum.deliveryOrderId;
                    return Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              title,
                              style: sealCardLblTextStyle,
                            ),
                            Text(
                              dataVal,
                              style: sealCardValTextStyle,
                            )
                          ],
                        ));
                  },
                ).toList(),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var sealOrderProvider = Provider.of<SealOrderProvider>(context);

    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: SafeArea(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              header(),
              TabBar(
                unselectedLabelColor: Colors.black,
                labelColor: Colors.blue[900],
                indicatorColor: Colors.blue[900],
                tabs: [
                  Tab(
                    child: Text(
                      'On Progress(${sealOrderProvider.assignDataQty})',
                      style: TextStyle(
                          color: Colors.blue[900], fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    // text: 'Available',
                    child: Text(
                      "Available(0)",
                      style: TextStyle(color: Colors.grey[500]),
                    ),
                  )
                ],
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.tab,
              ),
              Expanded(
                child: FutureBuilder<Map?>(
                    future: sealOrderProvider.getSealOrder(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return Center(
                          child: Container(
                            // color: Colors.white,
                            child: CircularProgressIndicator(),
                          ),
                        );
                      } else {
                        SealOrderModel dataMod = snapshot.data!['sealMod'];
                        return TabBarView(
                          children: [
                            ListView(
                              children: dataMod.data.assignmentData
                                  .map((data) => onProgressWidget(data))
                                  .toList(),
                            ),
                            Text('Person')
                          ],
                          controller: _tabController,
                        );
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
