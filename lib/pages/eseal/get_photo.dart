// ignore_for_file: prefer_const_constructors, unused_local_variable

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:future_job_anur/providers/seal_order_provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

enum ImageSourceType { gallery, camera }

class GetPhoto extends StatefulWidget {
  GetPhoto();

  @override
  GetPhotoState createState() => GetPhotoState();
}

class GetPhotoState extends State<GetPhoto> {
  var _image;
  var imagePicker;
  var type;

  GetPhotoState();

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    imagePicker = new ImagePicker();
  }

  @override
  Widget build(BuildContext context) {
    var sealOrderProvider = Provider.of<SealOrderProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Get Photo"),
      ),
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () async {
                  XFile image = await imagePicker.pickImage(
                      source: ImageSource.camera,
                      imageQuality: 50,
                      preferredCameraDevice: CameraDevice.front);

                  setState(() {
                    _image = File(image.path);
                  });
                },
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 0.9),
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                  ),
                  child: _image != null
                      ? Image.file(
                          _image,
                          fit: BoxFit.fill,
                        )
                      : Icon(
                          Icons.camera_alt_outlined,
                          color: Colors.grey[500],
                        ),
                ),
              ),
              ElevatedButton(
                  onPressed: () {
                    sealOrderProvider.sendData(_image);
                  },
                  child: Text("Execute"))
            ],
          ),
        ),
      ),
    );
  }
}
